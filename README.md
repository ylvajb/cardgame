# CardGame
### About
This is an assigment for IDATT2003, 2024. The program deals a hand consisting of 5 random cards, and then lets the user either check the hand or deal a new hand. 

Checking the hand will display the following:
* Sum of faces
* Flush
* Cards of heart
* Queen of Spades

The user can be dealt a new hand as long as there are still cards left in the deck. The deck can be reset.

### Executing program
##### Using an IDE:

Open the project in your IDE
Run the following command in terminal:


mvn javafx:run


##### Using Apache Maven:

Open the terminal and navigate to project folder
Run the following command:


mvn javafx:run