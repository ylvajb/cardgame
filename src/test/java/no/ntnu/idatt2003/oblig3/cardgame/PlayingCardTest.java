package no.ntnu.idatt2003.oblig3.cardgame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Test class for PlayingCard
 * @author ylvaj
 * @version 2024-03-10
 */
class PlayingCardTest {

  @Nested
  @DisplayName("PositiveTests")
  class MethodsDoNotThrowExceptions{
    @Test
    @DisplayName("Should create a card with valid suit and value")
    void shouldCreateCardWithValidSuitAndValue() {
      try {
        PlayingCard card = new PlayingCard('S', 1);
        Assertions.assertEquals('S', card.getSuit());
        Assertions.assertEquals(1, card.getFace());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return a string representation of the card")
    void shouldReturnStringRepresentationOfCard() {
      try {
        PlayingCard card = new PlayingCard('S', 1);
        Assertions.assertEquals("S1", card.getAsString());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return true when comparing two equal cards")
    void shouldReturnTrueWhenComparingTwoEqualCards() {
      try {
        PlayingCard card1 = new PlayingCard('S', 1);
        PlayingCard card2 = new PlayingCard('S', 1);
        Assertions.assertTrue(card1.equals(card2));
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }
  }

  @Nested
  @DisplayName("NegativeTests")
  class MethodsThrowExceptions{
    @ParameterizedTest
    @ValueSource(chars = {'A', 'B', 'Q', '-', 'E'})
    @DisplayName("Should throw IllegalArgumentException when creating card with invalid suit")
    void shouldThrowIllegalArgumentExceptionWhenCreatingCardWithInvalidSuit(char suit) {
      try {
        PlayingCard card = new PlayingCard(suit, 1);
        Assertions.fail("The test failed because the constructor did not throw an exception.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Parameter suit must be one of H, D, C or S", e.getMessage());
      } catch (Exception e) {
      Assertions.fail("The test failed because the constructor threw an exception. "
          + e.getMessage());
    }
    }

    @ParameterizedTest
    @ValueSource(ints = {0, 14, 15, 16, 17})
    @DisplayName("Should throw IllegalArgumentException when creating card with invalid value")
    void shouldThrowIllegalArgumentExceptionWhenCreatingCardWithInvalidValue(int value) {
      try {
        PlayingCard card = new PlayingCard('S', value);
        Assertions.fail("The test failed because the constructor did not throw an exception.");
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Parameter face must be a number between 1 to 13", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }
  }
}
