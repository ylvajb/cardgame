package no.ntnu.idatt2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

/**
 * Test class for DeckOfCards
 * @author ylvaj
 * @version 2024-03-10
 */
class DeckOfCardsTest {

  @Nested
  @DisplayName("PositiveTests")
  class MethodsDoNotThrowExceptions{
    @Test
    @DisplayName("Constructor should fill deck with cards when instanced")
    void constructorShouldFillDeckWithCardsWhenInstanced() {
      try {
        DeckOfCards deck = new DeckOfCards();
        Assertions.assertEquals(52, deck.getDeck().size());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @ParameterizedTest
    @ValueSource(ints = {6, 8, 20})
    @DisplayName("Should deal random hand when valid parameter is provided")
    void shouldDealRandomHandWhenValidParameterIsProvided(int n) {
      try {
        DeckOfCards deck = new DeckOfCards();
        List <PlayingCard> hand = deck.dealHand(n);
        Assertions.assertEquals(n, hand.size());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }

    }

    @Test
    @DisplayName("Should calculate sum of hand when valid parameter is provided")
    void shouldCalculateSumOfHandWhenValidParameterIsProvided() {
      try {
        DeckOfCards deck = new DeckOfCards();
        List <PlayingCard> hand = new ArrayList<>() {
          {
            add(new PlayingCard('S', 1));
            add(new PlayingCard('S', 2));
            add(new PlayingCard('S', 3));
            add(new PlayingCard('S', 4));
            add(new PlayingCard('S', 5));
          }
        };
        Assertions.assertEquals(15, deck.getSum(hand));
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return true when valid parameter is provided and flush is present")
    void shouldReturnTrueWhenValidParameterIsProvidedAndFlushIsPresent() {
      try {
        DeckOfCards deck = new DeckOfCards();
        List <PlayingCard> hand = new ArrayList<>() {
          {
            add(new PlayingCard('S', 1));
            add(new PlayingCard('S', 2));
            add(new PlayingCard('S', 3));
            add(new PlayingCard('S', 4));
            add(new PlayingCard('S', 5));
          }
        };
        Assertions.assertTrue(deck.checkFlush(hand));
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should return false when valid parameter is provided and no flush is present")
    void shouldReturnFalseWhenValidParameterIsProvidedAndNoFlushIsPresent() {
      try {
        DeckOfCards deck = new DeckOfCards();
        List <PlayingCard> hand = new ArrayList<>() {
          {
            add(new PlayingCard('S', 1));
            add(new PlayingCard('S', 2));
            add(new PlayingCard('S', 3));
            add(new PlayingCard('S', 4));
            add(new PlayingCard('H', 5));
          }
        };
        Assertions.assertFalse(deck.checkFlush(hand));
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }


  }

  @Nested
  @DisplayName("NegativeTests")
  class MethodsDoThrowExceptions{

    @ParameterizedTest
    @ValueSource(ints = {-4, 0, 4, 100})
    @DisplayName("Should not deal hand when invalid parameter is provided")
    void shouldNotDealHandWhenInvalidParametersAreProvided(int n) {
      try {
        DeckOfCards deck = new DeckOfCards();
        deck.dealHand(n);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Number of cards must be between 5 and 52", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should not deal hand when not enough cards to be dealt in deck")
    void shouldNotDealHandWhenNotEnoughCardsToBeDealtInStock() {
      try {
        DeckOfCards deck = new DeckOfCards();
        deck.dealHand(43);
        deck.dealHand(10);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Not enough cards in deck to deal 10 cards", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should not check flush when hand is null")
    void shouldNotCheckFlushWhenHandIsNull() {
      try {
        DeckOfCards deck = new DeckOfCards();
        deck.checkFlush(null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Hand cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

    @Test
    @DisplayName("Should not calculate sum of hand when hand is null")
    void shouldNotCalculateSumOfHandWhenHandIsNull() {
      try {
        DeckOfCards deck = new DeckOfCards();
        deck.getSum(null);
      } catch (IllegalArgumentException e) {
        Assertions.assertEquals("Hand cannot be null", e.getMessage());
      } catch (Exception e) {
        Assertions.fail("The test failed because the constructor threw an exception. "
            + e.getMessage());
      }
    }

  }

}