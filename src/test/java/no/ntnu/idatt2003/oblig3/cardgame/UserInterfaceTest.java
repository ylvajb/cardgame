package no.ntnu.idatt2003.oblig3.cardgame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * Test class for UserInterface
 * @author ylvaj
 * @version 2024-03-10
 */
class UserInterfaceTest {

  @Test
  @DisplayName("Should deal a hand when called")
  void shouldDealAHandWhenCalled() {
    try {
      UserInterface ui = new UserInterface();
      String[] hand = ui.start();
      Assertions.assertNotNull(hand);
    } catch (Exception e) {
      Assertions.fail("The test failed because the method threw an exception. "
          + e.getMessage());
    }
  }

  @Test
  @DisplayName("Should check the hand when called")
  void shouldCheckTheHandWhenCalled() {
    try {
      UserInterface ui = new UserInterface();
      String[] hand = ui.start();
      String[] results = ui.checkHand();
      Assertions.assertNotNull(results);
    } catch (Exception e) {
      Assertions.fail("The test failed because the method threw an exception. "
          + e.getMessage());
    }
  }

  @Test
  @DisplayName("Should return hand when resetting deck")
  void shouldReturnHandWhenResettingDeck() {
    try {
      UserInterface ui = new UserInterface();
      String[] hand = ui.start();
      Assertions.assertNotNull(ui.resetDeck());
    } catch (Exception e) {
      Assertions.fail("The test failed because the method threw an exception. "
          + e.getMessage());
    }
  }

}
