package no.ntnu.idatt2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
/**
 * Class representing a deck of cards
 * @author ylvaj
 * @version 2024-03-10
 */
public class DeckOfCards {
  private final List <PlayingCard> deck;
  private final char[] suits = { 'S', 'H', 'D', 'C' };
  private final Random random;

  /**
   * Constructor for DeckOfCards
   */
  public DeckOfCards() {
    this.deck = new ArrayList <>();
    this.random = new Random();
    resetDeck();
  }

  /**
   * Method for filling the deck with cards
   */
  public void resetDeck() {
    deck.clear();
    try{
      for (char suit: suits) {
        for (int i = 1; i <= 13; i++) {
          deck.add(new PlayingCard(suit, i));
        }
      }
    } catch (IllegalArgumentException e) {
      System.out.println("Could not create deck: " + e.getMessage());
    }
  }

  /**
   * Method for getting the deck
   * @return a list of cards
   */
  public List <PlayingCard> getDeck() {return this.deck;}

  /**
   * Method for dealing a hand of cards
   * @param n number of cards to deal
   * @return a list of cards
   * @throws IllegalArgumentException if n is less than 5 or greater than 52 or
   * if there is not enough cards in the deck to deal n cards
   */
  public  List <PlayingCard> dealHand(int n) throws IllegalArgumentException {
    //Check if number of cards is valid
    if (n < 5 || n > 52) {
      throw new IllegalArgumentException("Number of cards must be between 5 and 52");
    }
    else if (n > deck.size()) {
      throw new IllegalArgumentException("Not enough cards in deck to deal " + n + " cards");
    }
    List <PlayingCard> hand = new ArrayList<>();
    PlayingCard card;
    for (int i = 0; i < n; i++) {
      card = deck.get(random.nextInt(0,deck.size()));
      hand.add(card);
      deck.remove(card);
    }
    return hand;
  }

  /**
   * Method for checking the sum of a hand of cards
   * @param hand a list of cards
   * @return the sum of the cards in the hand
   * @throws IllegalArgumentException if hand is null
   */
  public int getSum(List <PlayingCard> hand) throws IllegalArgumentException {
    if (hand == null) throw new IllegalArgumentException("Hand cannot be null");
    int sum = 0;
    for (PlayingCard card : hand) {
      sum += card.getFace();
    }
    return sum;
  }

  /**
   * Method for checking if a hand of cards contains a flush
   * @param hand a list of cards
   * @return true if the hand contains a flush, false otherwise
   * @throws IllegalArgumentException if hand is null
   */
  public boolean checkFlush(List <PlayingCard> hand)throws IllegalArgumentException{
    if (hand == null) throw new IllegalArgumentException("Hand cannot be null");
    if (hand.size() >= 5){
      for (char suit : suits) {
        long numb = hand.stream().filter(card -> card.getSuit()==(suit)).count();
        if (numb >= 5) {
          return true;
        }
      }
    }
    return false;
  }

}
