package no.ntnu.idatt2003.oblig3.cardgame;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Class for the graphic user interface
 * @author ylvaj
 * @version 2024-03-10
 */
public class GraphicUserInterface extends Application {
  private List <TextField> textFields;
  private List <HBox> cards;
  private UserInterface ui;
  private static final  int WIDTH = 1000;
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) throws NullPointerException{
    cards = new ArrayList<>();
    textFields = new ArrayList<>();
    Group root = new Group();
    //Get stylesheet
    try {
      root.getStylesheets().add(Objects.requireNonNull(getClass().getResource("/styles.css")).toExternalForm());
    }
    catch (NullPointerException e) {
      throw new NullPointerException("Could not load stylesheet: " + e.getMessage());
    }
    //Set scene
    Scene scene = new Scene(root);
    root.getChildren().add(getLayout());

    //Set stage
    stage.setTitle("Card Game");
    stage.setScene(scene);
    stage.show();
    ui = new UserInterface();
    updateCards(ui.start());
  }

  /**
   * Method to get the main layout
   * @return a VBox with the main layout
   */
  private VBox getLayout(){
      //Create main layout
      VBox layout = new VBox();
      layout.getStyleClass().add("layout");
      layout.getChildren().addAll(getTopRow(),getCardContainer(),getButtonContainer());
      return layout;
    }

  /**
   * Gets the top row containing the reset and quit buttons
   * @return a VBox with the main layout
   */
  private HBox getTopRow() {
    HBox topRow = new HBox();
    topRow.getStyleClass().add("top-row");

    //Add buttons to the top row and set style
    Button button1 = new Button("Reset");
    Button button2 = new Button("Quit");
    button1.getStyleClass().add("top-row-button");
    button2.getStyleClass().add("top-row-button");

    //Add action listeners to the buttons
    button1.setOnAction(e -> {
      String[] hand = ui.resetDeck();
      if(hand.length > 0) {
        updateCards(hand);
        resetFields();
      }
    });
    button2.setOnAction(e -> ui.exit());

    topRow.getChildren().addAll(button1, button2);
    return topRow;
  }

  /**
   * Method to get the card container, containing 5 cards
   * @return a HBox with the card container
   */
  private HBox getCardContainer() {
    HBox cardContainer = new HBox();
    cardContainer.getStyleClass().add("card-container");
    cardContainer.setPrefWidth(WIDTH);

    //Add 5 cards to the card container
    for (int i = 0; i < 5; i++) {
      HBox card = new HBox();
      card.getStyleClass().add("card");
      card.getChildren().add(new Label("Card " + i));
      cards.add(card);
      cardContainer.getChildren().add(card);
    }
    return cardContainer;
  }

  /**
   * Method to get the button container, containing the deal hand and check hand buttons
   * @return a HBox with the button container
   */
  private HBox getButtonContainer() {
    HBox buttonContainer = new HBox();
    VBox buttonBox1 = new VBox();

    //Set button styles and width
    buttonContainer.setPrefHeight(200);
    buttonBox1.getStyleClass().add("button-box");
    buttonBox1.setPrefWidth((double) WIDTH /3);

    //Add buttons to the button containers
    Button button1 = new Button("Deal hand");
    Button button2 = new Button("Check hand");
    buttonBox1.getChildren().addAll(button1, button2);

    //Adding event handlers to the buttons
    button1.setOnAction(e -> {
      String[] hand = ui.dealHand();
      if(hand.length > 0) {
        updateCards(hand);
      }
    });
    button2.setOnAction(e -> {
      String [] fields = ui.checkHand();
      if(fields.length > 0) {
        updateFields(fields);
      }
    });

    buttonContainer.getChildren().addAll(buttonBox1, getTextBoxes());
    return buttonContainer;
  }

  /**
   * Method to get the text boxes
   * @return a HBox with the text boxes
   */
  private HBox getTextBoxes() {
    String[] labels = {"Sum of faces: ", "Flush: ", "Cards of hearts: ", "Queen of Spades: "};
    HBox container = new HBox();
    container.getStyleClass().add("button-box");
    VBox textBox1 = new VBox();
    VBox textBox2 = new VBox();

    //Create text containers for each label
    VBox textContainer1 = new VBox();
    VBox textContainer2 = new VBox();
    VBox textContainer3 = new VBox();
    VBox textContainer4 = new VBox();
    VBox[] textContainers = {textContainer1, textContainer2, textContainer3, textContainer4};

    //Set style
    textBox1.getStyleClass().add("fields-container");
    textBox2.getStyleClass().add("fields-container");
    textBox1.setPrefWidth((double) WIDTH /3);
    textBox2.setPrefWidth((double) WIDTH /3);

    //For each label, add a label and a text field to the text container
    for (int i = 0; i < textContainers.length; i++) {
      Label label = new Label(labels[i]);
      label.getStyleClass().add("text-label");
      TextField textField = new TextField();
      textField.getStyleClass().add("text-field");
      textField.setEditable(false);
      this.textFields.add(textField);
      textContainers[i].getChildren().addAll(label, textField);
    }

    //Add text containers to the text boxes
    textBox1.getChildren().addAll(textContainer1, textContainer2);
    textBox2.getChildren().addAll(textContainer3, textContainer4);
    container.getChildren().addAll(textBox1, textBox2);
    return container;
  }

  /**
   * Method to update the fields
   * @param fields a String[] with the fields
   */
  private void updateFields(String [] fields){
    //Update the fields with the result of the checkHand method
    for (int i = 0; i < fields.length; i++) {
      textFields.get(i).setText(fields[i]);
    }
  }

  /**
   * Method to update the cards
   * @param hand a String[] with the hand
   */
  private void updateCards(String[] hand){
    //Update the cards with the new hand
    //If there are more strings than cards, only update the amount of cards (HBoxes) that exist
    Label label;
    for (int i = 0; i < cards.size(); i++) {
      cards.get(i).getChildren().clear();
      label = new Label(hand[i]);
      if (hand[i].startsWith("H") || hand[i].startsWith("D")) {
        label.setStyle("-fx-text-fill: red");
      }
      else {
        label.setStyle("-fx-text-fill: black");
      }
      cards.get(i).getChildren().add(label);
    }
  }

  /**
   * Method to reset the text fields
   */
  private void resetFields(){
    //Reset the text fields
    for (TextField textField : textFields) {
      textField.setText("");
    }
  }

}


