package no.ntnu.idatt2003.oblig3.cardgame;
/**
 * Main class for the card game
 * @author ylvaj
 * @version 2024-03-10
 */
public class Main {
  public static void main(String[] args) {
    GraphicUserInterface.main(args);
  }
}