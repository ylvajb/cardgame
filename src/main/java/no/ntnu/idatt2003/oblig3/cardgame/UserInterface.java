package no.ntnu.idatt2003.oblig3.cardgame;

import java.util.ArrayList;
import java.util.List;
/**
 * Class for the user interface
 * @author ylvaj
 * @version 2024-03-10
 */
public class UserInterface {
  private final DeckOfCards deck;
  private List <PlayingCard> hand;
  private static final int HAND_SIZE = 5;

  /**
   * Constructor for UserInterface
   */
  public UserInterface() {
    this.deck = new DeckOfCards();
    this.hand = new ArrayList<>();
  }

  /**
   * Method to start the game
   * @return String[] with the hand
   */
  public String[] start() {
    //Start by dealing a hand, if not possible exit
    String[] handString = dealHand();
    if(handString == null){
      System.out.println("Could not deal hand. Exiting...");
      exit();
    }
    return handString;
  }

  /**
   * Method to check the cards
   * @return String[] with the hand
   */
  public String [] checkHand(){
    try{
      //Calculate results
      String flush = String.valueOf(deck.checkFlush(hand));
      String sum = String.valueOf(deck.getSum(hand));
      String queenOfSpades = String.valueOf(hand.contains(new PlayingCard('S', 12)));
      StringBuilder cardOfHearts = new StringBuilder();
      //Check for hearts and update string
      for (PlayingCard card : hand){
        if (card.getSuit() == 'H'){
          cardOfHearts.append(card.getAsString()).append(" ");
        }
      }
      if (cardOfHearts.isEmpty()){
        cardOfHearts = new StringBuilder("No hearts");
        }
      //Return results
      return new String[]{sum, flush, cardOfHearts.toString(), queenOfSpades};
      } catch (IllegalArgumentException e) {
      System.out.println("Could not check hand: " + e.getMessage());
    }
    return new String[0];
  }

  /**
   * Method to deal a hand, will deal HAND_SIZE amount of cards
   * @return String[] with the hand
   */
  public String [] dealHand(){
    String [] handString = new String[HAND_SIZE];
    try {
      //Deal han and return as string
      hand = deck.dealHand(HAND_SIZE);
      for (int i = 0; i < HAND_SIZE; i++) {
        handString[i] = hand.get(i).getAsString();
      }
      return handString;
    } catch (IllegalArgumentException e) {
      System.out.println("Could not deal hand: " + e.getMessage());
    }
    return new String[0];
  }

  /**
   * Method to reset the deck
   * @return String[] with the hand
   */
  public String [] resetDeck(){
    deck.resetDeck();
    return dealHand();
  }

  /**
   * Method to exit the game
   */
  public void exit(){
    System.exit(0);
  }
}
